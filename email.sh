#!/bin/sh
set -e
if [ -z "${2}" ]; then
	echo "Please... don't waste my time...
${0} email domain [password]"
	exit 1;
fi

CURRENT_DIR=`dirname "$0"`

${CURRENT_DIR} ./config.sh

EMAIL=${1}
DOMAIN=${2}
PASSWORD=`date +%s | sha256sum | head -c 20`

if [ ! -z "$3" ]; then
  PASSWORD=${3}
fi

EMAIL_INSERT="${1}@${2}"

Q_CREATEMAIL="INSERT INTO virtual_users (domain_id, password, email) SELECT id, ENCRYPT('${PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))) as password, '${EMAIL_INSERT}' as email  FROM virtual_domains WHERE name = '${DOMAIN}';"

echo ${Q_CREATEMAIL}
mysql -u ${MAIL_DB_USERNAME} ${MAIL_DB_NAME} --password="${MAIL_DB_PASSWORD}" -e "${Q_CREATEMAIL}"

echo "Created."
echo "Email: ${EMAIL_INSERT}"
echo "Password: ${PASSWORD}"
