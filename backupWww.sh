#!/bin/sh

echo "a iniciar..."
date
if [ "${1}" = "" ]
then
	echo "failed.."
else
	CURRENT_DIR=`dirname "$0"`
	. ${CURRENT_DIR}/config.sh

	set -v
	echo "remover backups na pasta last..."
	ls /var/backups/${1}/last/

	rm -r /var/backups/${1}/last/ftp_*
	mv /var/backups/${1}/ftp_* /var/backups/${1}/last/
	rm -r /var/backups/${1}/last/ftp_*

	ls /var/backups/${1}/

	echo "a gerar lista de ficheiros com menos de 2M"
	find ${NGINX_PATH} -type f -size -2M -not -path "*/tmp/*" -not -path "*/.git/*" > /tmp/backupfilelist.txt

	#run tar passing it the file list
	echo "a criar novo backup..."
	tar czf /var/backups/${1}/ftp_`date +%d%m%Y_%Hh%M`.tgz --files-from /tmp/backupfilelist.txt
	ls /var/backups/${1}/
	date
	echo "...fim"

fi
