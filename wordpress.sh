#!/bin/bash
set -e

if [ -z "$3" ]; then
	echo "Please... don't waste my time...
${0} domain databasename databasepass"
	exit 1;
fi

CURRENT_DIR=`dirname "$0"` 
. ${CURRENT_DIR}/helpers.sh 
. ${CURRENT_DIR}/config.sh

DOMAIN=${1}
DB_NAME=${2}
DB_USERNAME="${2}_usr"
DB_PASSWORD=${3}

DOMAIN_PATH=$(getDomainPath "${DOMAIN}")
INSTALLATION_PATH="${NGINX_PATH}${DOMAIN_PATH}";

SALT_WORDPRESS=$(curl https://api.wordpress.org/secret-key/1.1/salt/) # remove this, and generate ourselfs... fck wp.

# get wordpress
wget https://wordpress.org/latest.tar.gz

#INSTALLATION_PATH="/var/nginx"
# unpack wordpress
echo ${INSTALLATION_PATH}

tar zxvf latest.tar.gz -C ${INSTALLATION_PATH}
rm latest.tar.gz

cd ${INSTALLATION_PATH}
mv wordpress/* www/

rm -r wordpress

echo "
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to \"wp-config.php\" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '${DB_NAME}');

/** MySQL database username */
define('DB_USER', '${DB_USERNAME}');

/** MySQL database password */
define('DB_PASSWORD', '${DB_PASSWORD}');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

 ${SALT_WORDPRESS}


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
\$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

" >> ${INSTALLATION_PATH}www/wp-config.php

chown -R www-data ${INSTALLATION_PATH}
