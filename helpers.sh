#!/bin/bash
set -e

getDomainPath() {
	local domain=$1
	domainExtracted=`sed 's/.*\.\(.*\..*\)/\1/' <<< $domain`
	if [ "$domain" == "$domainExtracted" ]; then
		domainPath=$domain/
	else
		domainPath=$domainExtracted/subdomains/$domain/
	fi
	echo $domainPath
}