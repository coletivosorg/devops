# installation
Copy the file config.ini.sh to config.ini

`cp config.ini.sh config.ini`

edit the configuration to your needs

`nano config.ini`

## Arch linux
Edit the file `/etc/nginx/nginx.conf`

add the first line:
```
user http;
```

replace the last lines to look like:
```
include sites-enabled/*;

}
```

## debian flavour
Don't forget to change in config.sh `DISTRO="debian"`
# Usage

## Add domain to nginx
You create automatic nginx configurations, and install letsencrypt... create the folders
`./addDomain.sh example.com`

> A new folder will be created `/var/nginx/example.com/www` with the configuration file in `/etc/nginx/sites-available/example.com`

`./addDomain.sh subdomain.example.com`
> A new folder will be created `/var/nginx/example.com/subdomains/subdomain.example.com/www` with the configuration file in `/etc/nginx/sites-available/subdomain.example.com`


## Add mysql database to mysql
You create automatic nginx configurations, and install letsencrypt... create the folders
`$ ./addMysql.sh dbname`


it requires the mysql root password

`Enter password: [mysql password]`

>
and generates an output like this:

```bash
user: dbname_usr
database: dbname
passwd: b9c249b3909599d211b6

```

