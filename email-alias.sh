#!/bin/sh
set -e

if [ -z "${2}" ]; then
	echo "Please... don't waste my time...${0} source destination domain"
	exit 1;
fi

. ./config.sh

EMAIL=${1}
DESTINATION=${2}
DOMAIN=${3}

Q_CREATEMAIL="INSERT INTO virtual_aliases (domain_id, source, destination) SELECT id,'${EMAIL}', '${DESTINATION}' as email  FROM virtual_domains WHERE name = '${DOMAIN}';"

echo ${Q_CREATEMAIL}
mysql -u ${MAIL_DB_USERNAME} ${MAIL_DB_NAME} --password="${MAIL_DB_PASSWORD}" -e "${Q_CREATEMAIL}"

echo "Created."
echo "Email: ${EMAIL} redirects to ${DESTINATION}"
