#!/bin/bash
set -e

if [ -z "$1" ]; then
	echo "Please... don't waste my time...
$0 subdomain.maindomain.com"
	exit 1;
fi

DOMAIN=$1

CURRENT_DIR=`dirname "$0"`

${CURRENT_DIR} ./config.sh
${CURRENT_DIR} ./helpers.sh

echo "Setting stuff for $DOMAIN"

service_restart()
{
	echo "restarting the nginx"
	if [ "$DISTRO" == "ARCH" ]; then
		sudo systemctl restart nginx
	elif [ "$DISTRO" == "DEBIAN" ]; then
		sudo service nginx restart
	else
		echo "ups. insert code here, cant restart nginx.. "
		exit 0
	fi
}

nginx_config() {

	echo "
server{
	listen 80;
	server_name ${DOMAIN} www.${DOMAIN};";
	if [ "${WITH_SSL}" == "y" ]; then
		echo "
	rewrite ^(.*) https://${DOMAIN}\$1 permanent;
}
server{

	server_name ${DOMAIN} www.${DOMAIN};"
	fi

	echo "
	root ${DOMAIN_PATH};
	index index.php index.htm;

	access_log  /var/log/${DOMAIN}_access.log;
	error_log   /var/log/${DOMAIN}_error.log error;";

	echo "${CERTBOT}"

	nginx_php_fpm

	echo "	try_files \$uri \$uri/ @rewrite;
	location @rewrite {
		rewrite ^/(.*)$ /index.php?_url=/$1;
	}
}"

}

nginx_php_fpm(){
	if [ "${DISTRO}" == "DEBIAN" ]; then
		echo "
	location ~ \.php$ {
		try_files \$uri =404;
		fastcgi_pass unix:/var/run/php5-fpm.sock;
		fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
		fastcgi_index index.php;
		include fastcgi_params;
	}";
	fi
	if [ "${DISTRO}" == "ARCH" ]; then
		echo "
	location ~ \.php$ {
		fastcgi_pass   unix:/var/run/php-fpm/php-fpm.sock;
		fastcgi_index  index.php;
		include        fastcgi.conf;
	}";
	fi
}

DOMAIN_PATH=$(getDomainPath "$DOMAIN")
DOMAIN_PATH="${NGINX_PATH}${DOMAIN_PATH}www"

NGINX_CONFIG_PATH_AVAILABLE="${NGINX_CONFIG_PATH}sites-available/$DOMAIN"
NGINX_CONFIG_PATH_ENABLED="${NGINX_CONFIG_PATH}sites-enabled/$DOMAIN"

echo "Adding new domain ${NGINX_CONFIG_PATH_AVAILABLE} port 80"

WITH_SSL="n" nginx_config > ${NGINX_CONFIG_PATH_AVAILABLE}

echo "creating dir "
mkdir -p $DOMAIN_PATH
chown -R www-data:www-data $DOMAIN_PATH

ln -sf  ${NGINX_CONFIG_PATH_AVAILABLE}  ${NGINX_CONFIG_PATH_ENABLED}
echo "activated domain"

CERTBOT=""

if [ "${USE_LESTENCRYPT}" == "y" ]; then

	service_restart

	${CERTBOT_PATH}certbot-auto --nginx -d ${DOMAIN} -d www.${DOMAIN} --no-self-upgrade
	echo "certbot installing certificate and restarting nginx!.. done."
	echo "Writing config to: ${NGINX_CONFIG_PATH_AVAILABLE}"

	cp ${NGINX_CONFIG_PATH_AVAILABLE} temp.certbot
	CERTBOT=$(cat temp.certbot | grep "Certbot" | grep -v "}")

	WITH_SSL="y" nginx_config
	WITH_SSL="y" nginx_config > ${NGINX_CONFIG_PATH_AVAILABLE}

	rm temp.certbot
fi

service_restart

