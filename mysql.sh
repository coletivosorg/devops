#!/bin/sh
set -e

if [ -z "$1" ]; then
	echo "Please... don't waste my time...
${0} databasename"
	exit 1;
fi

CURRENT_DIR=`dirname "$0"` . ${CURRENT_DIR} ./config.sh

DATABASE_NAME=${1}
DATABASE_USR=${DATABASE_NAME}_usr
PASSWORD=`date +%s | sha256sum | head -c 20`

Q_CREATEDB="CREATE DATABASE ${DATABASE_NAME};"
Q_CREATEUSER="GRANT ALL ON ${DATABASE_NAME}.* TO '${DATABASE_USR}'@'localhost' IDENTIFIED BY '${PASSWORD}';"
Q_CREATEPERMS="FLUSH PRIVILEGES;"

SQL="${Q_CREATEDB}${Q_CREATEUSER}${Q_CREATEPERMS}"
mysql -u ${MYSQL_USR} -e "$SQL" -p${MYSQL_PASSWD}

echo "user: ${DATABASE_USR}"
echo "database: ${DATABASE_NAME}"
echo "passwd: ${PASSWORD}"
