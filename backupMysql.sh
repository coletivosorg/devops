#!/bin/sh
set -x

if [ "${1}" = "" ]
then
	echo "failed.."
else
	CURRENT_DIR=`dirname "$0"`
	. ${CURRENT_DIR}/config.sh

	rm -r /var/backups/${1}/last/sql_*
	mv /var/backups/${1}/sql_* /var/backups/${1}/last/

	#echo "mysqldump..."
	mysqldump --all-databases > /var/backups/${1}/sql_`date +%d%m%Y_%Hh%M`.sql
	gzip /var/backups/${1}/*.sql
fi
